(function () {
  "use strict";

  const script = document.createElement("script");
  script.innerHTML = `const block = e => {
    e.stopPropagation();
    e.stopImmediatePropagation();
  };

  document.addEventListener('contextmenu', e => block(e), true);`;

  (document.head || document.documentElement).appendChild(script);
})();
